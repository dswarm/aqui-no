import 'package:flutter/material.dart';

class ActionButton extends StatefulWidget {
  final String type;
  final String typeText;
  final Function() onPressed;
  final Color mainColor;
  final Color secondColor;

  const ActionButton(
      {Key key, this.type, this.typeText, this.onPressed, this.mainColor, this.secondColor})
      : super(key: key);

  @override
  _ActionButtonState createState() => _ActionButtonState();
}

class _ActionButtonState extends State<ActionButton> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Container(
          height: 70,
          margin: EdgeInsets.all(10),
          decoration: BoxDecoration(
              border: Border.all(color: widget.mainColor, width: 5),
              shape: BoxShape.circle),
          child: Container(
            decoration: BoxDecoration(
              border: Border.all(color: Colors.white, width: 2),
              shape: BoxShape.circle,
            ),
            child: RawMaterialButton(
              onPressed: widget.onPressed,
              splashColor: Colors.black,
              fillColor: widget.secondColor,
              elevation: 13.0,
              shape: CircleBorder(),
              child: Padding(
                padding: EdgeInsets.all(7.0),
                 child: Image(
                  image: AssetImage(widget.type),
                ),
              ),
            ),
          ),
        ),
        Center(child: Text(widget.typeText, style: TextStyle(fontSize: 14, color: Colors.white, letterSpacing: .12),),)
      ],
    );
  }
}
