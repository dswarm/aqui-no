import 'package:flutter/material.dart';

class SwarmButton extends StatefulWidget {
  final String title;
  final Function() onPressed;

  const SwarmButton({Key key, this.title, this.onPressed}) : super(key: key);

  @override
  _SwarmButtonState createState() => _SwarmButtonState();
}

class _SwarmButtonState extends State<SwarmButton> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
              border: Border.all(color: Colors.red, width: 10),
              shape: BoxShape.circle),
          child: Container(
            decoration: BoxDecoration(
              border: Border.all(color: Colors.white, width: 2),
              shape: BoxShape.circle,
            ),
            child: RawMaterialButton(
              onPressed: widget.onPressed,
              splashColor: Colors.black,
              fillColor: Colors.redAccent,
              elevation: 15.0,
              shape: CircleBorder(),
              child: Padding(
                padding: EdgeInsets.all(20.0),
                child: Text(
                  widget.title,
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 28,
                      fontFamily: "Montserrat"),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
