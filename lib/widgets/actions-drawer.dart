import 'package:flutter/material.dart';

import '../widgets/widgets.dart';

class ActionsDrawer extends StatefulWidget {
  ActionsDrawer({
    Key key,
  }) : super(key: key);
  _ActionsDrawerState createState() => _ActionsDrawerState();
}

class _ActionsDrawerState extends State<ActionsDrawer> {
  void _triggerReport() {
    //Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 15,
      child: Container(
        padding: EdgeInsets.all(15),
        child: GridView.count(
          mainAxisSpacing: 15,
          shrinkWrap: false,
          crossAxisCount: 3,
          children: <Widget>[
            Center(
              child: ActionButton(
                type: 'assets/images/assault.png',
                typeText: 'Assault',
                onPressed: _triggerReport,
                mainColor: Colors.blue,
                secondColor: Colors.blueAccent,
              ),
            ),
            Center(
              child: ActionButton(
                type: 'assets/images/car.png',
                typeText: 'Car Theft',
                onPressed: _triggerReport,
                mainColor: Colors.indigo,
                secondColor: Colors.indigoAccent,
              ),
            ),
            Center(
              child: ActionButton(
                type: 'assets/images/extortion.png',
                typeText: 'Extortion',
                onPressed: _triggerReport,
                mainColor: Colors.purple,
                secondColor: Colors.purpleAccent,
              ),
            ),
            Center(
              child: ActionButton(
                type: 'assets/images/home.png',
                typeText: 'Home Burglary',
                onPressed: _triggerReport,
                mainColor: Colors.pink,
                secondColor: Colors.pinkAccent,
              ),
            ),
            Center(
              child: ActionButton(
                type: 'assets/images/robbery.png',
                typeText: 'Robbery',
                onPressed: _triggerReport,
                mainColor: Colors.deepOrange,
                secondColor: Colors.deepOrangeAccent,
              ),
            ),
            Center(
              child: ActionButton(
                type: 'assets/images/medical.png',
                typeText: 'Medical',
                onPressed: _triggerReport,
                mainColor: Colors.amber,
                secondColor: Colors.amberAccent,
              ),
            ),
            Center(
              child: ActionButton(
                type: 'assets/images/suspicious.png',
                typeText: 'Suspicious',
                onPressed: _triggerReport,
                mainColor: Colors.black,
                secondColor: Colors.black12,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
