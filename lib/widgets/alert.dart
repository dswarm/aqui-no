import 'package:flutter/material.dart';

class SegurAlert extends StatelessWidget {
  final String title;
  final String header;
  final String body;
  final String buttonText;
  
  const SegurAlert(
    {
      Key key,
      this.title,
      this.header,
      this.body,
      this.buttonText
      }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: AlertDialog(
        title: Text(title),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text(header),
              Text(body),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text(buttonText),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }
}