import 'package:flutter/material.dart';

class SegurLogo extends StatelessWidget {
  final String variant;

  SegurLogo(this.variant);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(30.0),
      child: Image.asset('assets/images/segurIcon$variant.png'),
    );
  }
}
