import 'package:flutter/material.dart';
import '../widgets/widgets.dart';
import '../services/services.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _LoginPageState();
  }
}

class _LoginPageState extends State<LoginPage> {
  String _emailValue;
  String _passwordValue;
  final GlobalKey<FormState> _authFormKey = GlobalKey<FormState>();

  AuthService auth = AuthService();

  Widget _emailInputField() {
    return TextFormField(
      decoration: InputDecoration(
        icon: Icon(Icons.email),
        labelText: 'Email',
        labelStyle: TextStyle(
          fontFamily: 'Montserrat',
          fontWeight: FontWeight.w300,
        ),
        filled: false,
      ),
      keyboardType: TextInputType.emailAddress,
      validator: (String value) {
        if (value.isEmpty ||
            !RegExp(r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                .hasMatch(value)) {
          return 'Please enter a valid e-mail';
        }
        return '';
      },
      onSaved: (String value) {
        _emailValue = value;
      },
    );
  }

  Widget _passInputField() {
    return TextFormField(
      decoration: InputDecoration(
        icon: Icon(Icons.security),
        labelText: 'Password',
        labelStyle: TextStyle(
          fontFamily: 'Montserrat',
          fontWeight: FontWeight.w300,
        ),
        filled: false,
      ),
      obscureText: true,
      validator: (String value) {
        if (value.isEmpty || value.length < 5) {
          return 'Password is required and must be at least 5 characters';
        }
        return '';
      },
      onSaved: (String value) {
        _passwordValue = value;
      },
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/images/loginBg.png'),
                  fit: BoxFit.cover,
                  colorFilter: ColorFilter.mode(
                      Colors.black.withOpacity(.8), BlendMode.srcATop))),
          alignment: Alignment(0.0, 0.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Spacer(),
              SegurLogo('smaller'),
              Spacer(),
              Container(
                padding: EdgeInsets.fromLTRB(20, 0, 30, 0),
                child: Form(
                    autovalidate: true,
                    key: _authFormKey,
                    child: Column(
                      children: <Widget>[
                        _emailInputField(),
                        SizedBox(height: 10.0),
                        _passInputField(),
                        SizedBox(height: 30.0),
                        ButtonTheme(
                          minWidth: 240,
                          child: RaisedButton(
                            child: Text(
                              'Login',
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w600,
                                  fontFamily: 'Montserrat'),
                            ),
                            textColor: Colors.white,
                            color: Color(0xFFA90200),
                            splashColor: Colors.red,
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(10)),
                            padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                            onPressed: () async {
                              _authFormKey.currentState.save();
                              try {
                                var user = await auth.emailSignIn(
                                    _emailValue, _passwordValue);
                                if (user != null) {
                                  Navigator.pushReplacementNamed(
                                      context, '/home');
                                }
                              } catch (error) {
                                print(error);
                                showDialog(
                                    context: context,
                                    barrierDismissible: true,
                                    builder: (BuildContext build) {
                                      return SegurAlert(
                                        title: 'We Found an Error',
                                        header: error.code,
                                        body: 'Please try again',
                                        buttonText: 'Ok',
                                      );
                                    });
                              }
                            },
                          ),
                        ),
                      ],
                    )),
              ),
              Spacer(),
            ],
          ),
        ),
      ),
    );
  }
}
