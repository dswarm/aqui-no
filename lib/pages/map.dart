//import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter_background_geolocation/flutter_background_geolocation.dart'
    as bgeo;
import '../services/services.dart';
import '../widgets/widgets.dart';

class MapPage extends StatefulWidget {
  MapPage({Key key}) : super(key: key);

  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  BGService bg = BGService();
  SwarmService swarm = SwarmService();

  static double _lat;
  static double _lng;

  void openActionsDrawer(BuildContext ctx) {
    showModalBottomSheet(
        context: ctx,
        builder: (bCtx) {
          return ActionsDrawer();
        });
  }

  @override
  void initState() {
    
    bgeo.BackgroundGeolocation.getCurrentPosition(
      persist: false,
      desiredAccuracy: 30,
    ).then((bgeo.Location location) {
      var geolocation = location.toMap();
      print(geolocation['coords']['latitude']);
      setState(() {
        _lat = geolocation['coords']['latitude'];
        _lng = geolocation['coords']['longitude'];
      });
    });
    super.initState();
  }

  //Completer<GoogleMapController> _controller = Completer();
  GoogleMapController mapController;
  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
    /* mapController.setMapStyle(
        '[{"featureType": "road.arterial","elementType": "geometry","stylers": [{ "color": "#CCCCCC" }]},{"featureType": "landscape","elementType": "labels","stylers": [{ "visibility": "off" }]}]'); */
  }

  //GoogleMapController _mapController;

  static final CameraPosition _cameraPosition = CameraPosition(
    target: LatLng(_lat, _lng),
    zoom: 17.0,
  );
  @override
  Widget build(BuildContext context) {
    if (_lat == null) {
      return Scaffold(
        body: Center(
          child:Loader(),
        ),
      );
    } else {
      return Scaffold(
        body: Stack(
          children: <Widget>[
            GoogleMap(
              myLocationEnabled: true,
              mapType: MapType.normal,
              initialCameraPosition: _cameraPosition,
              onMapCreated: (GoogleMapController controller) {
                _onMapCreated(controller);
                //_initCameraPosition();
              },
            ),
            Positioned(
              child: Container(
                margin: EdgeInsets.only(bottom: 20),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      ReportButton(
                        icon: Icons.location_city,
                        title: "",
                        size: 15.0,
                        color: Colors.white,
                        onPressed: () {},
                      ),
                      SwarmButton(
                        title: "SOS",
                        onPressed: () {
                          swarm.triggerSwarm();
                        },
                      ),
                      ReportButton(
                        icon: Icons.report,
                        title: "",
                        size: 15.0,
                        color: Colors.white,
                        onPressed: () => openActionsDrawer(context),
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      );
    }
  }
}
