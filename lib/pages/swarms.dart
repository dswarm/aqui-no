//import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../services/services.dart';
import '../widgets/widgets.dart';
import '../models/models.dart';

class SwarmsPage extends StatelessWidget {
  SwarmsPage({Key key}) : super(key: key);
  final DatabaseService _db = DatabaseService();
  final MapService _map = MapService();

  @override
  Widget build(BuildContext context) {
    FirebaseUser user = Provider.of<FirebaseUser>(context);
    String userId;
    if (user != null) {
      userId = user.uid;
    }
    return StreamBuilder(
      stream: _db.getUserSwarms(userId),
      builder: (context, snap) {
        if (!snap.hasData) {
          return Center(
            child: Loader(),
          );
        }
        DataSnapshot snapshot = snap.data.snapshot;
        Map swarms = snapshot.value;
        return ListView.builder(
          itemCount: swarms.length,
          itemBuilder: (BuildContext ctxt, int index) {
            String key = swarms.keys.elementAt(index);
            Swarm swarm = Swarm.fromMap(swarms[key]);
            DateFormat dateFormat = DateFormat("yyyy/LLL/dd @ HH:mm");
            return Container(
              margin: EdgeInsets.symmetric(horizontal: 7, vertical: 7),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage(_map.getStaticMap(latitude: swarm.latitude, longitude: swarm.longitude)),
                  fit: BoxFit.cover,
                  colorFilter: ColorFilter.mode(
                      Colors.black.withOpacity(.4), BlendMode.srcATop),
                ),
                boxShadow: [
                  BoxShadow(color: Colors.black, offset: Offset(0,5),blurRadius: 6)
                ],
              ),
              padding: EdgeInsets.fromLTRB(5, 30, 0, 30),
              child: ListTile(
                leading: Icon(Icons.blur_circular, size: 50,),
                title: Text(
                  swarm.localityName,
                  style: Theme.of(context).textTheme.title,
                ),
                subtitle: Text(
                  dateFormat.format(swarm.createdAt),
                  style: Theme.of(context).textTheme.subtitle,
                ),
              ),
            );
          },
        );
      },
    );
  }
}