import 'package:flutter/material.dart';
import '../widgets/widgets.dart';

class StartPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment(0.0, 0.0),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/loginBg.png'),
            fit: BoxFit.cover,
            colorFilter: ColorFilter.mode(Colors.red.withOpacity(.4), BlendMode.srcATop)
          )
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Spacer(),
            SegurLogo('small'),
            Spacer(),
            ButtonTheme(
              minWidth: 240,
              child: RaisedButton(
                child: Text(
                  'New User',
                  style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                      fontFamily: 'Montserrat'),
                ),
                textColor: Colors.white,
                color: Colors.black,
                splashColor: Colors.red,
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(10)
                ),
                padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                onPressed: () => {
                  Navigator.pushNamed(context, '/onboarding')
                },
              ),
            ),
            SizedBox(height: 10),
            ButtonTheme(
              minWidth: 240,
              child: RaisedButton(
                child: Text(
                  'Login',
                  style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                      fontFamily: 'Montserrat'),
                ),
                textColor: Colors.white,
                color: Colors.black,
                splashColor: Colors.red,
                shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(10)
                ),
                padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                onPressed: () => {
                  Navigator.pushReplacementNamed(context, '/login')
                },
              ),
            ),
            Spacer(),
          ],
        ),
      ),
    );
  }
}