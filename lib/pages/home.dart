import 'package:flutter/material.dart';
import 'pages.dart';
import '../services/services.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomePageState();
  
  }
}

class _HomePageState extends State <HomePage> {
  BGService bg = BGService();
  @override
  void initState() { 
    super.initState();
    bg.configureBGeo();
  }
  
  int _selectedIndex = 2;
  
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  final List<Widget> _pages = [
    SettingsPage(),
    SwarmsPage(),
    MapPage(),
    NetworksPage(),
    ModulesPage()
  ];
  
  @override
  
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Aquí No'),
      ),
      body: _pages[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Icons.settings), title: Text('Ajustes')),
          BottomNavigationBarItem(
              icon: Icon(Icons.blur_circular), title: Text('Incidentes')),
          BottomNavigationBarItem(icon: Icon(Icons.home), title: Text('Inicio')),
          BottomNavigationBarItem(
              icon: Icon(Icons.group_work), title: Text('Redes')),
          BottomNavigationBarItem(
              icon: Icon(Icons.layers), title: Text('Módulos')),
        ],
        currentIndex: _selectedIndex,
        iconSize: 40,
        fixedColor: Colors.amber,
        onTap: _onItemTapped,
      ),
    );
  }
}