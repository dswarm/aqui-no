 
import 'package:flutter/material.dart';

class InitPage extends StatelessWidget {
  const InitPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: SizedBox(),
          ),
          Expanded(
            flex: 8,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  flex: 3,
                  child: Stack(
                    alignment: Alignment.center,
                    children: <Widget>[
                      SizedBox(
                        width: 285,
                        height: 285,
                        child: DecoratedBox(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(1000),
                              gradient: RadialGradient(
                                  center: const Alignment(0.05, 0.05),
                                  radius: 0.50,
                                  colors: <Color>[
                                    const Color(0xFFbd300a),
                                    const Color(0xFFA90200),
                                  ],
                                  stops: <double>[
                                    0.01,
                                    3.0
                                  ])),
                        ),
                      ),
                      ButtonTheme(
                        height: 250,
                        minWidth: 250,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(1000.0)),
                        child: RaisedButton(
                          child: Text('TO SWARM',
                              style: TextStyle(
                                  fontFamily: "Helvetica",
                                  fontSize: 40,
                                  fontWeight: FontWeight.w100)),
                          color: Colors.black,
                          textColor: Colors.white,
                          splashColor: Color(0xFFA90200),
                          onPressed: () => {},
                        ),
                      )
                    ],
                  ),
                ),
                Flexible(
                    flex: 1,
                    child: Container(
                      child: ButtonTheme(
                        height: 40,
                        minWidth: 300,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30)),
                        child: OutlineButton(
                          child: Text(
                            'San Miguel de Allende',
                            style: TextStyle(
                                fontFamily: "Montserrat",
                                fontSize: 20,
                                fontWeight: FontWeight.w600),
                          ),
                          borderSide: BorderSide(
                            color: Colors.black,
                            style: BorderStyle.solid,
                            width: 5.0,
                          ),
                          splashColor: Color(0xFFA90200),
                          highlightedBorderColor: Color(0xFFA90200),
                          highlightColor: Colors.white,
                          textColor: Colors.black,
                          onPressed: () => {},
                        ),
                      ),
                    )),
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: SizedBox(),
          )
        ],
      ),
    );
  }
}