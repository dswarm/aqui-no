import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import '../services/services.dart';
import '../widgets/widgets.dart';
import '../models/models.dart';


class NetworksPage extends StatelessWidget {
  NetworksPage({Key key}) : super(key: key);
  final DatabaseService _db = DatabaseService();

  @override
  Widget build(BuildContext context) {
    FirebaseUser user = Provider.of<FirebaseUser>(context);
    String userId;
    if (user != null) {
      userId = user.uid;
    }
    return StreamBuilder(
      stream: _db.getUserNetworks(userId),
      builder: (context, snap) {
        if (!snap.hasData) {
          return Center(
            child: Loader(),
          );
        }
        DataSnapshot snapshot = snap.data.snapshot;
        Map networks = snapshot.value;
        return ListView.builder(
          itemCount: networks.length,
          itemBuilder: (BuildContext ctxt, int index) {
            String key = networks.keys.elementAt(index);
            Network network = Network.fromMap(networks[key]);
            return Container(
              margin: EdgeInsets.symmetric(horizontal: 7, vertical: 7),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage(network.bgImage),
                  fit: BoxFit.cover,
                  colorFilter: ColorFilter.mode(
                      Colors.black.withOpacity(.1), BlendMode.srcATop),
                ),
                boxShadow: [
                  BoxShadow(color: Colors.black, offset: Offset(0,5),blurRadius: 6)
                ],
              ),
              padding: EdgeInsets.fromLTRB(5, 30, 0, 30),
              child: ListTile(
                leading: ClipOval(
                  child: Image.network(
                    network.logo,
                    width: 65,
                    height: 65,
                  ),
                ),
                title: Text(
                  network.name,
                  style: Theme.of(context).textTheme.title,
                ),
                subtitle: Text(
                  network.type,
                  style: Theme.of(context).textTheme.subtitle,
                ),
                trailing: ReportButton(
                        icon: FontAwesomeIcons.solidBell,
                        title: "",
                        size: 8.0,
                        color: Colors.amber,
                        onPressed: () {},
                      ),
              ),
            );
          },
        );
      },
    );
  }
}
