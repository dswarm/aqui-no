import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:firebase_auth/firebase_auth.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    FirebaseUser user = Provider.of<FirebaseUser>(context);
    return ListView(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            if (user.photoUrl != null)
              Container(
                padding: EdgeInsets.all(20),
                width: 100,
                height: 100,
                margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.white, width: 5),
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: NetworkImage(user.photoUrl),
                  ),
                ),
              ),
            Column(
              children: <Widget>[
                Text(
                  user.displayName,
                  style: Theme.of(context).textTheme.subhead,
                ),
                Text('pScore1'),
                Text('pScore2'),
                Text('pScore3'),
                Text('pScore4'),
              ],
            ),
          ],
        ),
        Divider(),
        ButtonBar(
          children: <Widget>[
            RaisedButton(
              child: Text('VerifyEmail'),
              onPressed: ()=>{},
            ),
            RaisedButton(
              child: Text('VerifyCel'),
              onPressed: ()=>{},
            ),
            RaisedButton(
              child: Text('CNet'),
              onPressed: ()=>{},
            ),
          ],
        ),
        Divider(),
        ListTile(
          leading: Icon(Icons.email),
          title: Text(user.email),
        ),
        ListTile(
          leading: Icon(Icons.phone),
          title: Text('Set your phone number'),
        ),
        ListTile(
          leading: Icon(Icons.credit_card),
          title: Text('Add Credit Card'),
        ),
        ListTile(leading: Icon(Icons.home), title: Text('Set home address'))
      ],
    );
  }
}