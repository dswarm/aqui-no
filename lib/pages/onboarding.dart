import 'package:flutter/material.dart';
import 'package:intro_views_flutter/Models/page_view_model.dart';
import 'package:intro_views_flutter/intro_views_flutter.dart';

class OnboardingPage extends StatelessWidget {
  final pages = [
    PageViewModel(
        pageColor: const Color(0xFFA90200),
        bubble: Image.asset('assets/images/miniIcon.png'),
        body: Text(
          'SEGUR app is a state of the art emergency response system',
        ),
        title: Text(
          '',
        ),
        textStyle: TextStyle(fontFamily: 'Montserrat', color: Colors.white, fontSize: 20.0),
        mainImage: Image.asset(
          'assets/images/segurIconsmall.png',
          height: 300.0,
          width: 300.0,
          alignment: Alignment.center,
        )),
    PageViewModel(
      pageColor: const Color(0xFF000000),
      iconImageAssetPath: 'assets/images/miniIcon.png',
      body: Text(
        'In order for SEGUR to work we need access to your location.',
      ),
      title: Text(''),
      mainImage: Image.asset(
        'assets/images/slide2.png',
        height: 200.0,
        width: 200.0,
        alignment: Alignment.center,
      ),
      textStyle: TextStyle(fontFamily: 'Montserrat', color: Colors.white, fontSize: 20.0),
    ),
    PageViewModel(
      pageColor: const Color(0xFF00A7A9),
      iconImageAssetPath: 'assets/images/miniIcon.png',
      body: Text(
        'With SEGUR you will never be alone, there are 150 peers around you',
      ),
      title: Text(''),
      mainImage: Image.asset(
        'assets/images/slide3.png',
        height: 200.0,
        width: 200.0,
        alignment: Alignment.center,
      ),
      textStyle: TextStyle(fontFamily: 'Montserrat', color: Colors.white, fontSize: 22.0),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    
    return IntroViewsFlutter(
              pages,
              onTapDoneButton: () {
                Navigator.popAndPushNamed(context, '/');
              },
              pageButtonTextStyles: TextStyle(
                color: Colors.white,
                fontSize: 18.0,
              ),
            );
  }
}