import 'package:flutter/material.dart';

class  Network {
  final String id;
  final String description;
  final double latitude;
  final double longitude;
  final double perimeter;
  final String locality;
  final String localityName;
  final String name;
  final String logo;
  final String bgImage;
  final Map members;
  final String owner;
  final Map settings;
  final String type;

  Network({
    @required this.id,
    @required this.description,
    @required this.latitude,
    @required this.longitude,
    @required this.perimeter,
    @required this.locality,
    @required this.localityName,
    @required this.name,
    this.bgImage,
    this.logo,
    @required this.members,
    @required this.owner,
    @required this.settings,
    @required this.type,
    });

      factory Network.fromMap(Map data){
      return Network (
        id: data['networkId'] ?? 'id',
        description: data['description'] ?? 'Security Network',
        latitude: data['latitude'] ?? 0.0,
        longitude: data['longitude'] ?? 0.0,
        perimeter: data['perimeter'] ?? 0.0,
        locality: data['locality'] ?? "San Miguel de Allende",
        localityName: data['localityName'] ?? 'San Miguel de Allende',
        name: data['name'] ?? 'Community',
        bgImage: data['bgImage'] ?? 'https://www.app.segurapp.io/assets/img/social/network-hero.png',
        logo: data['logo'] ?? 'https://www.app.segurapp.io/assets/img/teem_icon.png',
        members: data['members'] ?? {},
        owner: data['owner'] ?? 'me',
        settings: data['settings'] ?? {},
        type: data['type'] ?? 'community',
        );
    }
}