import 'package:flutter/material.dart';

class Swarm {
  final DateTime createdAt;
  final String owner;
  final double latitude;
  final double longitude;
  final String locality;
  final String localityName;
  final Map bindings;
  final Map location;
  final Map users;

  Swarm({
    @required this.createdAt, 
    @required this.owner,
    @required this.latitude, 
    @required this.longitude,
    @required this.locality,
    @required this.localityName,
    @required this.bindings,
    @required this.location,
    this.users 
    });

    factory Swarm.fromMap(Map data){
      return Swarm(
        createdAt: DateTime.fromMillisecondsSinceEpoch(data['createdAt']) ?? 1532909363893,
        owner: data['owner'] ?? "",
        latitude: data['latitude'] ?? 0.0,
        longitude: data['longitude']?? 0.0,
        locality: data['locality'] ?? 'San Miguel de Allende',
        localityName: data['localityName'] ?? 'San Miguel de Allende',
        bindings: data['bindings'] ?? {},
        location: data['location'] ??{},
        users: data['users'] ?? {}
      );
    }
}