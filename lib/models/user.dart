import 'package:flutter/material.dart';

class UserData {
  final String locality;
  final String localityId;
  final Map devices;
  final Map networks;
  final Map pScore;
  final Map swarms;
  final bool webAccess;
  final bool validated;

  UserData({
    @required this.locality,
    @required this.localityId,
    @required this.devices,
    @required this.networks,
    @required this.pScore,
    @required this.swarms,
    @required this.webAccess, 
    @required this.validated,
    });
    
    factory UserData.fromMap(Map data){
      return UserData (
        locality: data['locality'] ?? 'San Miguel de Allende',
        localityId: data['localityId'] ?? 'San Miguel de Allende',
        devices: data['devices'] ?? {},
        networks: data['communities']?? {},
        pScore: data['pScore']?? {},
        swarms: data['swarms']?? {},
        webAccess: data['webAccess'] ?? false,
        validated: data['validated'] ?? false,
        );
    }
}