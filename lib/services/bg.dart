
import 'package:flutter_background_geolocation/flutter_background_geolocation.dart' as bg;


class BGService {

  Future<dynamic> getCurrentLoc() {
    return bg.BackgroundGeolocation.getCurrentPosition(
        persist: true, // <-- do not persist this location
        desiredAccuracy: 50, // <-- desire best possible accuracy
        timeout: 30000, // <-- wait 30s before giving up.
        samples: 3);
  }

  void configureBGeo()async {


    try{
      
    } catch (error){
      print(error);
    }
     
     bg.BackgroundGeolocation.onLocation((bg.Location location) {
      print('[location] - $location');
    });

    // Fired whenever the plugin changes motion-state (stationary->moving and vice-versa)
    bg.BackgroundGeolocation.onMotionChange((bg.Location location) {
      print('[motionchange] - $location');
    });

    // Fired whenever the state of location-services changes.  Always fired at boot
    bg.BackgroundGeolocation.onProviderChange((bg.ProviderChangeEvent event) {
      print('[providerchange] - $event');
    });

    ////
    // 2.  Configure the plugin
    //
    bg.BackgroundGeolocation.ready(bg.Config(
        desiredAccuracy: bg.Config.DESIRED_ACCURACY_HIGH,
        distanceFilter: 10.0,
        stopOnTerminate: false,
        startOnBoot: true,
        debug: false,
        logLevel: bg.Config.LOG_LEVEL_VERBOSE,
        reset: true,
        /* url: 'https://us-central1-emilio-lncsyt.cloudfunctions.net/bgGeolocation',
        params: {
        } */
    )).then((bg.State state) {
      if (!state.enabled) {
        ////
        // 3.  Start the plugin.
        //
        bg.BackgroundGeolocation.start();
      }
    });
  }
 

  void stopGeolocation (){
    bg.BackgroundGeolocation.stop();
  }

  void startGeolocation (){
    bg.BackgroundGeolocation.start();
  }  
}