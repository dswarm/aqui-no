
//import 'package:firebase_database/firebase_database.dart';

import '../services/db.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import '../models/models.dart';

/// Static global state. Immutable services that do not care about build context. 
class Global {
  // App Data
  static final String title = 'Emilio App';

  // Services
  static final FirebaseAnalytics analytics = FirebaseAnalytics();

    // Data Models
  static final Map models = {
     UserData: (data) => UserData.fromMap(data),
     Network: (data) => Network.fromMap(data),
   /*Job: (data) => Job.fromMap(data),
    Organization: (data) => Organization.fromMap(data), */

  };

  // Firestore References for Writes
  static final UserDataNode<UserData> userRef = UserDataNode<UserData>();
 
}
