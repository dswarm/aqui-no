import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

//import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:async';


class AuthService with ChangeNotifier{

    final FirebaseAuth _auth = FirebaseAuth.instance;
    //final Firestore _db = Firestore.instance; 

  Future<FirebaseUser> get getUser => _auth.currentUser();
  Stream<FirebaseUser> get user => _auth.onAuthStateChanged;
  
  Future<AuthResult> emailSignIn(email, password) async {
    return _auth.signInWithEmailAndPassword(email: email, password: password);
  }

  /* Future<void> updateUserData(FirebaseUser user) {
    DocumentReference userRef = _db.collection('users').document(user.uid);

    return userRef.updateData({
      'uid': user.uid,
      'email': user.email,
      'name' : user.displayName,
      'avatar': user.photoUrl
    });
  } */

  Future<void> signOut(){
    return _auth.signOut();
  }

}