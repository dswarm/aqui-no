import 'package:firebase_auth/firebase_auth.dart';
import 'package:geocoder/geocoder.dart';
import 'services.dart';
import 'package:flutter_geofire/flutter_geofire.dart';

class SwarmService {
  BGService _bg = BGService();
  AuthService _auth = AuthService();
  DatabaseService _db = DatabaseService();
  String pathToReference = "swarmGeoQ/San Miguel de Allende";
  Future<dynamic> triggerSwarm()async{
    try{
      FirebaseUser _user = await _auth.getUser;
      var _location = await _bg.getCurrentLoc();
      final coordinates = new Coordinates(_location.coords.latitude, _location.coords.longitude);
      var _localityData = await Geocoder.local.findAddressesFromCoordinates(coordinates);
      // Form Swarm data
      Map<String, dynamic> swarmData = {
        "bindings": {
          "reason": "unknown",
          "active" : true,
          "confirmed" : false,
          "participants": 1,
          "swarmPos" : {
            "lat": _location.coords.latitude,
            "lng": _location.coords.longitude
          }
        },
        "latitude": _location.coords.latitude,
        "longitude": _location.coords.longitude,
        "locality": _localityData.first.locality,
        "localityName": _localityData.first.locality,
        "owner": _user.uid,
        "location": _location.toMap(),
        "users": {},
        "createdAt": DateTime.now().millisecondsSinceEpoch.toString(),
      };
      // Push data to database;
      var _key = await _db.setSwarm(swarmData);
      print(_key);
      // Set event to geofire
      Geofire.initialize(pathToReference);
      bool response = await Geofire.setLocation(_key, _location.coords.latitude, _location.coords.longitude);
      print(response);
    } catch(e){
      print(e);
      throw(e);
    }

  }
}