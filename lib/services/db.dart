//import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:rxdart/rxdart.dart';
//import '../models/models.dart';
import '../services/services.dart';
import 'dart:async';

final FirebaseDatabase _db = FirebaseDatabase.instance;
final FirebaseAuth _auth = FirebaseAuth.instance;

final user = _auth.currentUser();
class DatabaseService {
  
  Stream <dynamic> getUserNetworks (userId)  {
    return _db.reference().child('users/$userId/communities').onValue;
  }
  Stream <dynamic> getUserSwarms (userId){
    return _db.reference().child('users/$userId/swarms').orderByKey().onValue;
  }
  Future<String>setSwarm(data)async{
    String key = _db.reference().child("swarms").child("San Miguel de Allende").push().key;
     _db.reference().child("swarms").child("San Miguel de Allende").child(key).set(data);
     return key;
  }
}

class Node<T> {
  final String path;
  DatabaseReference ref;

  Node({this.path}) {
    ref = _db.reference();
  }

  Future<T> getData() {
    return ref.once().then((v) => Global.models[T](v) as T);
  }

  Stream<T> streamData() {
    return ref.onChildAdded.map((v) => Global.models[T](v) as T);
  }

  Future<void> upsert(Map data) {
    return ref.set(Map<String, dynamic>.from(data));
  }
}

class UserDataNode <T>{
  

  UserDataNode();

  /* Stream<T> get nodeStream{
    return Observable(_auth.onAuthStateChanged).switchMap((user){
      if(user != null){
        Node<T> node = Node<T>(path: 'users/${user.uid}');
        return node.streamData();
      } else {
        return Observable<T>.just(null);
      }
    });
  } */
}
